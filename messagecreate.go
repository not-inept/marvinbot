package main

import (
	"fmt"
	"math/rand"
	"strings"

	"github.com/Necroforger/gomoji"
	"github.com/bwmarrin/discordgo"
)

var treeReacts = []string{
	":deciduous_tree:",
	":seedling:",
	":herb:",
	":leaves:",
}

var plantyWords = []string{
	"leaves",
	"root",
	"seed",
	"plant",
	"marv",
	"tree",
	"pot",
	"stem",
	"bud",
}

var plantyActions = []string{
	"sits",
	"waits",
	"watches",
	"listens",
	"grows",
	"suns",
	"photosynthesizes",
	"contemplates",
	"philosophizes",
	"converts CO2 to O2",
}

var plantyDescriptions = []string{
	"quietly",
	"serenely",
	"with great sorrow",
	"intensely",
	"mysteriously",
	"happily",
	"plantily",
	"quickly",
	"with great stillness",
	"gracefully",
	"hungrily",
	"thirstily",
}

func isPlanty(content string) bool {
	content = strings.ToLower(content)
	for _, word := range plantyWords {
		if strings.Contains(content, word) {
			return true
		}
	}
	return false
}

func chooseRandom(slice []string) string {
	return slice[rand.Intn(len(slice))]
}

func shareThoughts(s *discordgo.Session, m *discordgo.MessageCreate) {
	message := fmt.Sprintf("_%s %s_", chooseRandom(plantyActions), chooseRandom(plantyDescriptions))
	s.ChannelMessageSend(m.ChannelID, message)
}

func myMention(s *discordgo.Session) string {
	// Mentions show up as <@id> in messages where they aren't parsed out
	return fmt.Sprintf("<@%s>", s.State.User.ID)
}

// MessageCreate handles the Message Create discord API event
func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if s.State.User.ID == m.Author.ID {
		return
	}
	if isPlanty(m.Content) || strings.Contains(m.Content, myMention(s)) {
		reaction := gomoji.Format(chooseRandom(treeReacts))
		s.MessageReactionAdd(m.ChannelID, m.ID, reaction)
	}
	if rand.Float32() > .9 {
		shareThoughts(s, m)
	}
}
