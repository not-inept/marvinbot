package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/not-inept/marvinbot/util"
)

func main() {
	logrus.Info("Starting up MarvinBot.")
	config := util.LoadConfig("marvinbot.json")

	discord, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		logrus.Fatal("Error creating Discord session,", err)
	}
	discord.AddHandler(MessageCreate)
	err = discord.Open()
	if err != nil {
		logrus.Fatal("Error opening connection,", err)
	}

	// Wait here until CTRL-C or other term signal is received.
	logrus.Infof("MarvinBot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}
